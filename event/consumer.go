package event

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"
	"strings"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
)

type Consumer struct {
	conn *amqp.Connection
}

type Task struct {
	Handler string          `json:"handler"`
	Payload json.RawMessage `json:"payload"`
}

func (c *Consumer) StartListener(serviceName string) {
	// Open a channel
	channel, err := c.OpenChannel()
	failOnError(err, "Failed to open a channel")
	defer channel.Close()

	err = channel.ExchangeDeclare(
		"service_tasks", // name
		"topic",         // type
		true,            // durable
		false,           // auto-deleted
		false,           // internal
		false,           // no-wait
		nil,             // arguments
	)

	failOnError(err, "Failed to declare an exchange")

	// Declare the queue
	q, err := channel.QueueDeclare(
		fmt.Sprintf("%s_queue", serviceName), // queue name
		true,                                 // durable
		false,                                // delete when unused
		false,                                // exclusive
		false,                                // no-wait
		nil,                                  // arguments
	)
	failOnError(err, "Failed to declare a queue")

	err = channel.QueueBind(
		q.Name,                           // queue name
		fmt.Sprintf("%s.*", serviceName), // routing key
		"service_tasks",                  // exchange name
		false,
		nil,
	)
	failOnError(err, "Failed to bind the queue")

	// Set up a consumer
	msgs, err := channel.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	// Create a channel to handle signal interrupts for graceful shutdown
	forever := make(chan bool)

	// Goroutine to process messages
	go func() {
		for m := range msgs {
			go c.handleMessage(m, channel)
		}
	}()

	//log.Printf(" [*] Waiting for messages on %s. To exit press CTRL+C", queueName)
	<-forever
}

func (c *Consumer) Connect() error {
	var counts int64
	var backOff = 1 * time.Second
	var connection *amqp.Connection

	for {
		c, err := amqp.Dial("amqp://guest:guest@rabbitmq")
		if err != nil {
			fmt.Println("RabbitMQ not yet ready")
			counts++
		} else {
			fmt.Println("Connected to RabbitMQ")
			connection = c
			break
		}

		if counts > 5 {
			fmt.Println(err)
			return err
		}

		backOff = time.Duration(math.Pow(float64(counts), 2)) * time.Second
		time.Sleep(backOff)
		continue
	}

	c.conn = connection

	return nil
}

func (c *Consumer) CloseConnection() error {
	err := c.conn.Close()
	if err != nil {
		return err
	}

	return nil
}

func (c *Consumer) handleMessage(message amqp.Delivery, channel *amqp.Channel) {
	var task Task
	_ = json.Unmarshal(message.Body, &task)

	handler := strings.Replace(task.Handler, ".", "/", 1)
	serviceURL := fmt.Sprintf("http://%s", handler)

	request, err := http.NewRequest("POST", serviceURL, bytes.NewBuffer(task.Payload))
	if err != nil {
		log.Println(err)
	}

	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{}

	response, err := client.Do(request)
	if err != nil {
		log.Println(err)
	}

	defer response.Body.Close()
	//if response.StatusCode == http.StatusAccepted {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	err = channel.PublishWithContext(
		ctx,
		"",
		message.ReplyTo,
		false,
		false,
		amqp.Publishing{
			CorrelationId: message.CorrelationId,
			ContentType:   "application/json",
			Body:          []byte("VSE HOROSHO!"), //TODO SEND something useful
		},
	)
	log.Println(err)
	//}

	//channel
	//ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	//defer cancel()

	//channel.PublishWithContext(
	//	ctx,
	//	""
	//	)
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func (c *Consumer) OpenChannel() (*amqp.Channel, error) {
	channel, err := c.conn.Channel()
	if err != nil {
		return nil, err
	}

	return channel, nil
}
