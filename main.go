package main

import (
	"log"
	"strings"

	"listener/event"

	"github.com/spf13/viper"
)

type App struct {
}

func main() {
	initViper()

	services := viper.GetString("SERVICES")
	slice := strings.Split(services, ",")

	consumer := event.Consumer{}
	err := consumer.Connect()
	if err != nil {
		panic(err)
	}
	defer func() {
		err = consumer.CloseConnection()
	}()

	// Print the resulting substrings
	for _, serviceName := range slice {
		go consumer.StartListener(serviceName)
	}

	// Block main goroutine to keep the program running
	select {}
}

func initViper() {
	viper.SetConfigFile(".env")
	viper.SetConfigType("env")

	// Add the current directory to the paths to look for the config file
	viper.AddConfigPath(".")

	// Automatically replace `.` with `_` in environment variable keys
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)

	// Read the config file
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file: %v", err)
	}

	// Bind environment variables
	viper.AutomaticEnv()

	// Read the SERVICES variable

}
